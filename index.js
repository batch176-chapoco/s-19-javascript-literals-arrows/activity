//NUMBER 3
let getCube = 10 ** 3

//NUMBER 4
console.log(`The cube of 10 is ${getCube}`);

//NUMBER 5
let address = ["341 Centro St.", "Brgy.Pantoc", "Meycauayan", "Bulacan"];

//NUMBER 6
console.log(`I live at ${address[0]}, ${address[1]}, ${address[2]}, ${address[3]}.`)

//NUMBER 7
let animal = {
    anName: "Sabby",
    anArms: "4 Arms",
    anLegs: "6 Legs",
    anWeight: "1.5 Kgs"
}

//NUMBER 8
console.log(`${animal.anName} has ${animal.anArms}, ${animal.anLegs}, and weighs ${animal.anWeight}.`)

//NUMBER 9

let arrayNum = [1, 2, 3, 4, 5]
let i = arrayNum[0];

arrayNum.forEach((number) => console.log(number))

class Dog {
    constructor(name, age, breed) {
        this.name = name,
            this.age = age,
            this.breed = breed
    }
}

let newDog = new Dog()

newDog.name = "Saab"
newDog.age = 2
newDog.breed = "Golden Retriever"

console.log(newDog);